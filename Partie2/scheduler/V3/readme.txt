Objectif : 

- Faire clignoter la LED rouge toutes les 300ms.
- envoyer en boucle un message "coucou" sur le port série toutes les 100ms.
- Afficher le message "Salut" sur l’écran LCD.
- envoyer en boucle le caractère '@' sur le port série.
- Mettre en place l'ordonnanceur avec ressource partagée

Contexte : 

- La fonction ISR joue le rôle de scheduler, chaque 20ms on ordonnance une nouvelle tâche.
- On déclare 3 variables globales : CREATED, ACTIVE et SUSPENDED pour gérer l'état d'éxecution des tâches.
- On utilise les deux macros SAVE_CONTEXT et RESTORE_CONTEXT pour sauvegarder la valeur des différents registres du micro-contrôleur.
- les primitives take_serial et release_serial prennent le rôle de sémaphore. take_serial vérifie si le port série est disponible, si oui le prend, si non la     tâche l'exécutant est suspendue. release_serial qui rend le port série et, si besoin, rend active la tâche suspendue.
- On utilise la variable serial_port_status sert de suivis de l'état du port série, si égal à 1 le port est occupé sinon si égal à 0 le port est libre.
- La fonction task_serial2 a pour rôle d'envoyer le caractère '@' sur le port série.
- On fait appelle au primitives take_serial et release_serial dans les fonctions task_serial et release_serial pour gérer l'accès au port série.

Résultat & observation : 

- Les tâches s'executent correctement et ne s'interrompt pas.
- Le message "coucou" s'écrit entièrement avec retour à la ligne à chaque caractère.
- Le caractère '@' s'écrit en concurrence avec le message "coucou" sur le port série. On obtient donc le message "@coucou".
- Lorsque le message "coucou" s'ecrit sur le port série, le caractère '@' ne peut pas être envoyer sur le port série.
- L'écran LCD affiche le message "Salut" en boucle en vidant la sortie avec un clear.


Conclusion : 

- On partage le port série pour les deux tâches task_serial et task_serial2. On a donc un système de sémaphore simplifié.