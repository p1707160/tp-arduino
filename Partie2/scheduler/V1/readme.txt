Objectif : 

- Faire clignoter la LED rouge toutes les 300ms.
- envoyer en boucle un message sur le port série toutes les 100ms.
- Afficher le message sur l’écran LCD.

Contexte : 

- La fonction ISR joue le rôle de scheduler, chaque 20ms on ordonnance une nouvelle tâche.

Résultat & observation : 

- Les tâches se reset à chaque interruption.
- Le message "coucou" ne s'ecrit pas entièrement sur le port série. On a que le premier caractère qui s'affiche.
- L'écran LCD affiche le message "Salut" sans faire de clear on a donc le message "Salut" qui sature l'écran.

Conclusion : 

- Sauvegarde de contexte : Il est crucial d'implémenter une sauvegarde de contexte dans votre ISR. 
  Cela permettra aux tâches de reprendre là où elles se sont arrêtées lors de la dernière interruption. 
  Ceci peut être réalisé en sauvegardant les registres et les variables d'état avant de changer de tâche.