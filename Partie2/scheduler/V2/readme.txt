Objectif : 

- Faire clignoter la LED rouge toutes les 300ms.
- envoyer en boucle un message "coucou" sur le port série toutes les 100ms.
- Afficher le message "Salut" sur l’écran LCD.

- Mettre en place la sauvegarde de contexte.

Contexte : 

- La fonction ISR joue le rôle de scheduler, chaque 20ms on ordonnance une nouvelle tâche.
- On utilise les deux macros SAVE_CONTEXT et RESTORE_CONTEXT pour sauvegarder la valeur des différents registres du micro-contrôleur.

Résultat & observation : 

- Les tâches s'executent correctement et ne s'interrompt pas.
- Le message "coucou" s'écrit entièrement avec retour à la ligne à chaque caractère.
- L'écran LCD affiche le message "Salut" en boucle en vidant la sortie avec un clear.

Conclusion : 

- La sauvegarde de contexte permet à l'ordonnanceur de pouvoir éxecuter chaque tâches entièrement sans les reinitialiser à chaque interruptions.